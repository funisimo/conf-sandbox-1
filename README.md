# Conf-Sandbox

Conf-Sandbox has LEMP stack configuration for Ubuntu or Debian.

## Install Ubuntu or Debian

Update distribution

	apt-get update && apt-get -y upgrade && apt-get -y dist-upgrade

Install main packages

	# Tools
	apt-get install git mc htop

	# Nginx
	apt-get install nginx-light

	# PHP + PHP-FPM
	apt-get install php5-fpm php5-curl php5-mysqlnd php-pear php5-dev php5-json php5-xsl php5-xdebug libxml2-utils

	# MySQL
	mysql-server mysql-client

## Clone Conf-Sandbox repository

	git clone https://bitbucket.org/IngussNeilands/conf-sandbox.git

Copy-Paste config files from cloned repository to proper locations.

## Enjoy

